//
//  ViewController.swift
//  ScrollViewDemo
//
//  Created by Mark Maged on 8/29/17.
//  Copyright © 2017 Mark Maged. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var skyImg: UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.addGesture()
        
        for index in 0...2
        {
            let image = UIImage(named: "icon\(index).png")
            let imageView = UIImageView(image: image)
            
            self.scrollView.addSubview(imageView)
            
            let newX = self.scrollView.frame.width / 2 + (CGFloat(index) * self.scrollView.frame.size.width)
            
            imageView.frame = CGRect(x: newX - 75, y: (self.scrollView.frame.height / 2) - 75, width: 150, height: 150)
        }
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.frame.size.width * 3, height: self.scrollView.frame.height)
        self.scrollView.clipsToBounds = false
    }
    
    func addGesture()
    {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapAction(gesture:)))
        tapGesture.numberOfTapsRequired = 1
        
        self.skyImg.addGestureRecognizer(tapGesture)
    }
    
    func tapAction(gesture: UITapGestureRecognizer)
    {
        let touchLocation = gesture.location(in: self.view)
        let scrollWidth = self.scrollView.frame.width
        let currentOffset = self.scrollView.contentOffset
        
        if touchLocation.x < self.scrollView.frame.minX
        {
            let xNewOffset = currentOffset.x - scrollWidth;
            
            if xNewOffset >= 0
            {
                UIView.animate(withDuration: 0.5,
                               animations: {
                                self.scrollView.contentOffset = CGPoint(x: xNewOffset, y: currentOffset.y)
                })
            }
        }
        else if touchLocation.x > self.scrollView.frame.maxX
        {
            let xNewOffset = currentOffset.x + scrollWidth;
            
            if xNewOffset < self.scrollView.contentSize.width
            {
                UIView.animate(withDuration: 0.5,
                               animations: {
                                self.scrollView.contentOffset = CGPoint(x: xNewOffset, y: currentOffset.y)
                })
            }
        }
    }
}
